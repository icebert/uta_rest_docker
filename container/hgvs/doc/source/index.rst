.. _index:

====
hgvs
====

.. include:: ../description.txt

|build_status| | `Source <https://bitbucket.org/biocommons/hgvs>`_ | `Documentation <http://pythonhosted.org/hgvs/>`_ | `Discuss <https://groups.google.com/forum/#!forum/hgvs-discuss>`_ | `Issues <https://bitbucket.org/biocommons/hgvs/issues?status=new&status=open>`_


Contents
--------

.. toctree::
   :maxdepth: 2

   intro
   quick_start
   using_hgvs
   reference
   development
   changelog
   license


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |build_status| image:: https://drone.io/bitbucket.org/biocommons/hgvs/status.png
  :target: https://drone.io/bitbucket.org/biocommons/hgvs
  :align: middle

