FROM ubuntu:14.04.3
MAINTAINER HGVS Contributors
LABEL Description="hgvs UTA REST interface server" Vendor="Biocommons"

RUN apt-get update && apt-get install -y \
    build-essential \
    python \
    python-dev \
    libpq-dev \
    mercurial

ADD https://bootstrap.pypa.io/get-pip.py /home/hgvs/

RUN python /home/hgvs/get-pip.py && rm -f /home/hgvs/get-pip.py


# Install hgvs
COPY hgvs /home/hgvs/hgvs

RUN cd /home/hgvs/hgvs && \
    python setup.py install && \
    pip install configparser && \
    pip install Flask && \
    pip install flask-restful && \
    rm -rf /home/hgvs/hgvs


# Install and config uWSGI
ADD http://projects.unbit.it/downloads/uwsgi-2.0.11.1.tar.gz /home/hgvs/

RUN cd /home/hgvs && tar zxf uwsgi-2.0.11.1.tar.gz && \
    cd uwsgi-2.0.11.1 && make && \
    mkdir /home/hgvs/uwsgi && mv uwsgi /home/hgvs/uwsgi/ && \
    rm -rf /home/hgvs/uwsgi-2.0.11.1.tar.gz /home/hgvs/uwsgi-2.0.11.1

COPY conf.ini /home/hgvs/uwsgi/


# Start uWSGI
COPY start_uwsgi.sh /home/hgvs/

EXPOSE 9090 9191

CMD ["/bin/bash", "/home/hgvs/start_uwsgi.sh"]

